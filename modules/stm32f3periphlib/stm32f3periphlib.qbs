import qbs 1.0
import qbs.FileInfo
import qbs.Probes
import qbs.Environment

Module {
    Depends { name: "cpp" }

    property stringList stm32f3periphSrcSearchPaths:["~/projects/cpplibs/mcu/stm32/STM32F30x_DSP_StdPeriph_Lib_V1.2.3"]
    property stringList stm32f3periphLibSearchPaths:["~/projects/cpplibs/mcu/stm32/stm32f3stdperiphlib/",
                                                     "~/projects/cpplibs/mcu/stm32/stm32f3periphlib"]
    property string MCU

    property string __home:{
        var home
        if(qbs.hostOS.contains("linux"))
            home = Environment.getEnv("HOME") /*qbs.getEnv("HOME")*/
        else
            home = "d:"
        return home
    }

    property stringList __searchPaths:{
        var res = [];

        for (var i = 0; i < stm32f3periphSrcSearchPaths.length; ++i){
            var s = ""
            if (stm32f3periphSrcSearchPaths[i].startsWith("~")){
                s = stm32f3periphSrcSearchPaths[i].replace("~", __home)
            }
            else{
                s = stm32f3periphSrcSearchPaths[i]
            }
            res.push(s)
        }
        return res
    }
    property stringList __libSearchPaths:{
        var res = [];
        for (var i = 0; i < stm32f3periphLibSearchPaths.length; ++i){
            var s = ""
            if (stm32f3periphLibSearchPaths[i].startsWith("~")){
                s = stm32f3periphLibSearchPaths[i].replace("~", __home)
            }
            else{
                s = stm32f3periphLibSearchPaths[i]
            }
            res.push(s)
        }
        return res
    }

    Probes.PathProbe{
        id:path_of_stm32f3_src
        names:["stm32f30x.h"]
        pathSuffixes:["", "Libraries/CMSIS/Device/ST/STM32F30x/Include"]
        platformPaths:{
            var res = []
            res = res.concat(__searchPaths)
            return res
        }
    }
    property string periph_lib_path:{
        var pp = "/_____NO_PATH_FOUND______"
        if(path_of_stm32f3_src.found){
            pp = path_of_stm32f3_src.path
            var ccc = pp.split("/")
            var rrr = []
            if(ccc.length > 6){
                for (var i = 0; i < ccc.length - 6; ++i){
                    rrr.push(ccc[i])
                }
                pp = rrr.join("/")
            }
        }
        return pp
    }

    property string lib_name:{
        var name_base = "stm32f3periph_"
        name_base = name_base + MCU
        if(qbs.buildVariant == "debug")
            name_base = name_base + "d"
        return name_base
    }
    Probes.PathProbe{
      id:path_of_stm32f3periph_lib
      names:["lib"+lib_name+".a"]
      pathSuffixes:["", "lib"]
      platformPaths:{
          var res = []
          res = res.concat(__libSearchPaths)
          return res
      }
    }
    Probes.PathProbe{
      id:path_of_config
      names:["stm32f30x_conf.h"]
      pathSuffixes:["", "config", "conf"]
      platformPaths:{
          var res = []
          res = res.concat(__libSearchPaths)
          return res
      }
    }
    //stm32f30x_conf.h
    property string libPath: {
        var path = "/path_not_found"
        if(path_of_stm32f3periph_lib.found)
            path = path_of_stm32f3periph_lib.path
        return path
    }
    property string configPath: {
        var path = "/config_not_found"
        if(path_of_config.found)
            path = path_of_config.path
        return path
    }


    
    Properties {
        condition:true
        cpp.includePaths: {
            var paths = []
            paths = paths.concat([ periph_lib_path + "/Libraries/CMSIS/Include/",
                                   periph_lib_path + "/Libraries/STM32F30x_StdPeriph_Driver/inc",
                                   periph_lib_path + "/Libraries/CMSIS/Device/ST/STM32F30x/Include/",
                                   configPath,
                                  ])
            return paths;
          }

        cpp.staticLibraries:{
                var libs = base
                var name_base = "stm32f3periph_"
                name_base = name_base + MCU
                if(qbs.buildVariant == "debug")
                    name_base = name_base + "d"
                
                libs = libs.concat([name_base])
                return libs
            }

        cpp.libraryPaths:{
            var lib_path = [libPath]
            return lib_path
        }
        cpp.defines:[MCU, "__FPU_PRESENT=1", "USE_STDPERIPH_DRIVER","USE_FULL_ASSERT","ARM_MATH_CM4"]


        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections"]

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++14","-fno-exceptions","-fno-rtti"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat(["-std=gnu11"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(["-specs=nano.specs","-specs=rdimon.specs"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }
    }

    property stringList MCU_FLAGS: ["-mthumb","-mcpu=cortex-m4"]
    property stringList FPU_FLAGS: ["-mfloat-abi=hard", "-mfpu=fpv4-sp-d16" ]
}
