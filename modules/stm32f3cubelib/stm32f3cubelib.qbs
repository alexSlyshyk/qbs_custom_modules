import qbs 1.0
import qbs.FileInfo
import qbs.Probes

Module {
    Depends { name: "cpp" }
    property string MCU
    property stringList searchPaths:["~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.3.0",
        "~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.4.0"]
    property stringList libSearchPaths:["~/projects/cpplibs/mcu/stm32/stm32cube_F3_lib",
        "~/projects/cpplibs/mcu/stm32/stm32cube_f3_lib"]

    property string __home:{
        var home
        if(qbs.hostOS.contains("linux"))
            home = qbs.getEnv("HOME")
        else
            home = "d:"
        return home
    }
    property stringList __searchPaths:{
        var res = [];
        for (var i = 0; i < searchPaths.length; ++i){
            var s = ""
            if (searchPaths[i].startsWith("~")){
                s = searchPaths[i].replace("~", __home)
            }
            else{
                s = searchPaths[i]
            }
            res.push(s)
        }
        return res
    }
    property stringList __libSearchPaths:{
        var res = [];
        for (var i = 0; i < libSearchPaths.length; ++i){
            var s = ""
            if (libSearchPaths[i].startsWith("~")){
                s = libSearchPaths[i].replace("~", __home)
            }
            else{
                s = libSearchPaths[i]
            }
            res.push(s)
        }
        return res
    }

    Probes.PathProbe{
        id:path_of_stm32f3cube
        names:["stm32f3xx.h"]
        pathSuffixes:["", "Drivers/CMSIS/Device/ST/STM32F3xx/Include"]
        platformPaths:{
            var res = []
            res = res.concat(__searchPaths)
            return res
        }
    }
    property string periph_lib_path:{
        var pp = "/NO_WAY"
        if(path_of_stm32f3cube.found){
            pp = path_of_stm32f3cube.path
            var ccc = pp.split("/")
            var rrr = []
            if(ccc.length > 6){
                for (var i = 0; i < ccc.length - 6; ++i){
                    rrr.push(ccc[i])
                }
                pp = rrr.join("/")
            }
        }
        return pp
    }

    property string lib_name:{
        var name_base = "stm32f3cube_"
        name_base = name_base + MCU
        if(qbs.buildVariant == "debug")
            name_base = name_base + "d"
        return name_base
    }
    Probes.PathProbe{
      id:path_of_stm32f3cube_lib
      names:["lib"+lib_name+".a"]
      pathSuffixes:["", "lib"]
      platformPaths:{
          var res = []
          res = res.concat(__libSearchPaths)
          return res
      }
    }
    Probes.PathProbe{
      id:path_of_config
      names:["stm32f3xx_hal_conf.h"]
      pathSuffixes:["", "config"]
      platformPaths:{
          var res = []
          res = res.concat(__libSearchPaths)
          return res
      }
    }
    //stm32f3xx_hal_conf.h
    property string libPath: {
        var path = "/path_not_found"
        if(path_of_stm32f3cube_lib.found)
            path = path_of_stm32f3cube_lib.path
        return path
    }
    property string configPath: {
        var path = "/path_not_found"
        if(path_of_config.found)
            path = path_of_config.path
        return path
    }

    
    Properties {
        condition:true
        cpp.includePaths: {
            var paths = []
            paths = paths.concat([
                                  periph_lib_path + "/Drivers/CMSIS/Include/",
                                  periph_lib_path + "/Drivers/STM32F3xx_HAL_Driver/Inc",
                                  periph_lib_path + "/Drivers/CMSIS/Device/ST/STM32F3xx/Include/",
                                  configPath,
                                  ])
            return paths;
          }

        cpp.staticLibraries:{
                var libs = base    
                libs = libs.concat([lib_name])
                return libs
        }
        cpp.libraryPaths:{
            var lib_path = [libPath]
            return lib_path
        }

        cpp.defines:[MCU, "__FPU_PRESENT=1","USE_HAL_DRIVER","USE_FULL_ASSERT","ARM_MATH_CM4"]

        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections"]

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++14","-fno-exceptions","-fno-rtti"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat(["-std=gnu11"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            flags = flags.concat(["-specs=nano.specs","-specs=rdimon.specs"])
            return flags
        }
    }//Propertyes


        property stringList MCU_FLAGS: ["-mthumb","-mcpu=cortex-m4"]
        property stringList FPU_FLAGS: ["-mfloat-abi=hard", "-mfpu=fpv4-sp-d16" ]
}
