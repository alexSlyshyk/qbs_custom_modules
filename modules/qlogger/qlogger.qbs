import qbs 1.0
import qbs.FileInfo
import qbs.Environment

Module {
    Depends { name: "cpp" }
    Depends { name: "Qt.core" }

    property string qtVersion: Qt.core.version
    property string libPath: {
        var path
        if(qbs.hostOS.contains("linux")){
            path = "~/projects/cpplibs/qlogger/lib/"
            if(qbs.architecture === "x86")
                path = path + "linux-g++/"
            if(qbs.architecture === "x86_64")
                path = path + "linux-g++-64/"
        }
        else if(qbs.hostOS.contains("windows")){
            path = "d:/projects/cpplibs/qlogger/lib/"
            if(qbs.toolchain.contains("mingw") && qbs.architecture === "x86")
                path = path + "win32-g++/"
            else if(qbs.toolchain.contains("msvc"))
                path = path + "win32-msvc2015/"
            else
                path = path + qbs.toolchain + "/"
        }

        path = path + qtVersion

        return path
    }
    
    Properties {
        condition:true
        cpp.includePaths: {
            var paths = ["~/projects/cpplibs/qlogger/include","d:/projects/cpplibs/qlogger/include"];
            return paths;
          }

        cpp.dynamicLibraries:{
                var dyn_libs = base
                var lib_suffix = ""
                if(qbs.buildVariant == "debug")
                     lib_suffix = "d"
                dyn_libs = dyn_libs.concat(["qlogger"+lib_suffix])

                return dyn_libs
            }
        }

        cpp.libraryPaths:{
            var lib_path = [libPath]
            return lib_path
        }

        cpp.rpaths : {
        var home = ""
        if(qbs.hostOS.contains("linux"))
            home = Environment.getEnv("HOME")
        else
            home = "d:"
        var lp = libPath
        lp = lp.replace("~", home)
                var rpath = [lp]
                return rpath
            }
}
