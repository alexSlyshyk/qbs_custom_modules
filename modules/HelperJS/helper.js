
function dirName(path){
    var path_components = path.split("/")
    var dn = ""
    if(path_components.length === 0)
        dn = "."
    else
        dn = path_components.slice(0, -1).join("/")

    return dn
}
