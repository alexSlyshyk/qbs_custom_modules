import qbs
import qbs.ModUtils
import qbs.FileInfo
import qbs.TextFile
import qbs.Process

Module {
    id: hex
    property bool with_bin:true
    property bool with_hex:true
    property bool with_listing:true

    property stringList bin_args: []

    //additionalProductTypes: ["hex","bin","listing"]
    additionalProductTypes:{
        var addt = []
        if(with_bin)
            addt = addt.concat(["bin"])
        if(with_hex)
            addt = addt.concat(["hex"])
        if(with_listing)
            addt = addt.concat(["listing"])
        return addt
    }

    Depends{name:"cpp"}
    property string compilerPath : {return FileInfo.path(cpp.compilerPath)}

    property string targetName : product.targetName

    property string baseName:{
        var bs = targetName.split('.');
        if(bs.lenght == 1)
            return bs[0]

        bs.pop()

        return bs.join(".")
    }

    Rule {
        condition:product.moduleProperty("hex", "with_hex")
        id: obj_hex
        inputs: ["application"]
        multiplex: true
        Artifact {
            filePath: product.moduleProperty("hex", "baseName")+".hex"
            fileTags: ["hex"]
        }

        prepare: {
                var commands = []
                var cppPath = ModUtils.moduleProperty(product, "compilerPath")
                var targetName = product.moduleProperty("hex", "targetName");
                var objcopy = cppPath + "/arm-none-eabi-objcopy"

                var elf_path = product.buildDirectory+"/"+targetName
                if(targetName.endsWith(".elf")) {
                    targetName = targetName.substr(0,targetName.length - 4)
                }

                var args = ["-O" , "ihex",elf_path,product.buildDirectory+"/"+targetName+".hex"]
                var cmd = new Command(objcopy,args);

                cmd.workingDirectory = cppPath
                cmd.description = "Processing " + targetName + ".hex";
                cmd.highlight = "filegen";

                commands = commands.concat([cmd])
                return commands;
            }
    }

    Rule {
        condition:product.moduleProperty("hex", "with_bin")
        id: obj_bin
        inputs: ["application"]
        multiplex: true
        Artifact {
            filePath: product.moduleProperty("hex", "baseName")+".bin"
            fileTags: ["bin"]
        }

        prepare: {
                var commands = []
                var cppPath = ModUtils.moduleProperty(product, "compilerPath")
                var targetName = product.moduleProperty("hex", "targetName");
                var bin_args   = product.moduleProperty("hex", "bin_args");
                var objcopy = cppPath + "/arm-none-eabi-objcopy"
                var objsize = cppPath + "/arm-none-eabi-size"
                var objdump = cppPath + "/arm-none-eabi-objdump"

                var elf_path = product.buildDirectory+"/"+targetName
                if(targetName.endsWith(".elf")) {
                    targetName = targetName.substr(0,targetName.length - 4)
                }

                var args_bin = bin_args
                args_bin = args_bin.concat(["--strip-unneeded" ,"-O", "binary",elf_path,product.buildDirectory+"/"+targetName+".bin"])
                var cmd_bin = new Command(objcopy,args_bin);

                cmd_bin.workingDirectory = cppPath
                cmd_bin.description = "Processing " + targetName + ".bin";
                cmd_bin.highlight = "filegen";

                commands = commands.concat([cmd_bin])
                return commands;
            }
    }

    Rule {
        condition:product.moduleProperty("hex", "with_listing")
        id: obj_lst
        inputs: ["application"]
        multiplex: true
        Artifact {
            filePath: product.moduleProperty("hex", "baseName")+".lst"
            fileTags: ["listing"]
        }

        prepare: {
                var commands = []
                var cppPath = ModUtils.moduleProperty(product, "compilerPath")
				var cppPrefix = product.moduleProperty("cpp", "toolchainPrefix");
                var targetName = product.moduleProperty("hex", "targetName");
				if(!cppPrefix)
					cppPrefix = ""
				var objsize = cppPath + "/"+cppPrefix+"size"
                var objdump = cppPath + "/"+cppPrefix+"objdump"

                var elf_path = product.buildDirectory+"/"+targetName
                if(targetName.endsWith(".elf")) {
                    targetName = targetName.substr(0,targetName.length - 4)
                }

                var cmd_file = new JavaScriptCommand();
                cmd_file.description = "Creating listing file" ;
                cmd_file.objdump = objdump
                cmd_file.output = product.buildDirectory+"/"+targetName+".lst"
                cmd_file.elf = elf_path
                cmd_file.sourceCode = function() {
                    var pr = new Process();
                    pr.exec(objdump,["-dC",elf], true);
                    var text = pr.readStdOut();
                    var bash = new TextFile(output,TextFile.WriteOnly);
                    bash.write(text)
                    bash.close();
                };

                commands = commands.concat([cmd_file]);

                var args_size = ["-d" ,elf_path]
                var cmd_size = new Command(objsize,args_size);

                cmd_size.workingDirectory = cppPath
                cmd_size.description = " ----- View size ----- ";
                cmd_size.highlight = "filegen";
                commands = commands.concat([cmd_size]);

                return commands;
            }
    }
}
