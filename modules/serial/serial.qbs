import qbs 1.0
import qbs.FileInfo

Module {
    Depends { name: "cpp" }
    property string libPath: {
        var path
        if(qbs.hostOS.contains("linux")){
            path = "~/projects/cpplibs/serial/lib/linux/"
            path = path + qbs.architecture
        }
        else if(qbs.hostOS.contains("windows")){
            path = "d:/projects/cpplibs/serial/lib/"
            if(qbs.toolchain.contains("mingw") && qbs.architecture === "x86")
                path = path + "win32-g++/"
            path = path + qbs.architecture
        }
        return path
    }
    
    Properties {
        condition:true
        cpp.includePaths: {
            var paths = []
            if(qbs.hostOS.contains("linux")){
                paths = paths.concat(["~/projects/cpplibs/serial/serial/include"])
            }
            if(qbs.hostOS.contains("windows")){
                paths = paths.concat(["D:/projects/cpplibs/serial/serial/include"])
            }
            return paths;
          }

        cpp.dynamicLibraries:{
                var dyn_libs = base
                var lib_suffix = ""
                if(qbs.buildVariant == "debug")
                     lib_suffix = "d"
                dyn_libs = dyn_libs.concat(["serial"+lib_suffix])

                return dyn_libs
            }
        }

        cpp.libraryPaths:{
            var lib_path = [libPath]
            return lib_path
        }

        cpp.rpaths : {
		var home = ""
		if(qbs.hostOS.contains("linux"))
			home = qbs.getEnv("HOME")
		else
			home = "d:"
		var lp = libPath
		lp = lp.replace("~", home)
                var rpath = [lp]
                return rpath
            }
}
