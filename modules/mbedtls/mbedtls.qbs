import qbs 1.0
import qbs.FileInfo

Module {
    Depends { name: "cpp" }
    property string libPath: {
        var path
        if(qbs.hostOS.contains("linux")){
            path = "~/projects/cpplibs/mbedtls/lib/"
        }
        else if(qbs.hostOS.contains("windows")){
            path = "d:/projects/cpplibs/mbedtls/lib/"
        }
        return path
    }
    
    Properties {
            condition:true
            cpp.includePaths: {
                var paths = []
                if(qbs.hostOS.contains("linux")){
                    paths = paths.concat(["~/projects/cpplibs/mbedtls/mbedtls-1.3.11/include"])
                }
                if(qbs.hostOS.contains("windows")){
                    paths = paths.concat(["d:/projects/cpplibs/serial/serial/include"])
                }
                return paths;
            }

            cpp.staticLibraries:{
                var tn = "mbedtls"
                if( cpp.compilerPathByLanguage["cpp"].contains("arm-none"))
                    tn = tn + "-none"
                else if(qbs.targetOS.contains("linux"))
                    tn = tn+"-linux"
                else if (qbs.targetOS.contains("windows"))
                    tn = tn+"-win"

                tn = tn+"-"+qbs.architecture+"-mt"
                if(qbs.buildVariant == "debug")
                    tn = tn+"d"
                return tn;
            }
        cpp.libraryPaths:{
            var lib_path = [libPath]
            return lib_path
        }
    }

}
