import qbs 1.0
import qbs.FileInfo
import qbs.Probes
import qbs.Environment

Module {
    Depends { name: "cpp" }

    property stringList stm32f4periphSrcSearchPaths:["~/projects/cpplibs/mcu/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.6.0/",
        "~/projects/cpplibs/mcu/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.6.1/"]
    property stringList stm32f4periphLibSearchPaths:["~/projects/cpplibs/mcu/stm32/STM32F4xx_DSP_StdPeriph_Lib/","D:/projects/cpplibs/mcu/stm32/stm32f4periphlib"]
    property string MCU

    property string __home:{
        var home
        if(qbs.hostOS.contains("linux"))
            home = Environment.getEnv("HOME")
        else
            home = "d:"
        return home
    }

    property stringList __searchPaths:{
        var res = [];

        for (var i = 0; i < stm32f4periphSrcSearchPaths.length; ++i){
            var s = ""
            if (stm32f4periphSrcSearchPaths[i].startsWith("~")){
                s = stm32f4periphSrcSearchPaths[i].replace("~", __home)
            }
            else{
                s = stm32f4periphSrcSearchPaths[i]
            }
            res.push(s)
        }
        return res
    }
    property stringList __libSearchPaths:{
        var res = [];
        for (var i = 0; i < stm32f4periphLibSearchPaths.length; ++i){
            var s = ""
            if (stm32f4periphLibSearchPaths[i].startsWith("~")){
                s = stm32f4periphLibSearchPaths[i].replace("~", __home)
            }
            else{
                s = stm32f4periphLibSearchPaths[i]
            }
            res.push(s)
        }
        return res
    }

    Probes.PathProbe{
        id:path_of_stm32f4_src
        names:["stm32f4xx.h"]
        pathSuffixes:["", "/Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
        platformPaths:{
            var res = []
            res = res.concat(__searchPaths)
            return res
        }
    }
    property string periph_lib_path:{
        var pp = "/NO_WAY"
        if(path_of_stm32f4_src.found){
            pp = path_of_stm32f4_src.path
            var ccc = pp.split("/")
            var rrr = []
            if(ccc.length > 6){
                for (var i = 0; i < ccc.length - 6; ++i){
                    rrr.push(ccc[i])
                }
                pp = rrr.join("/")
            }
        }
        return pp
    }

    property string lib_name:{
        var name_base = "stm32f4periph_"
        name_base = name_base + MCU
        if(qbs.buildVariant == "debug")
            name_base = name_base + "d"
        return name_base
    }
    Probes.PathProbe{
      id:path_of_stm32f4periph_lib
      names:["lib"+lib_name+".a"]
      pathSuffixes:["", "lib"]
      platformPaths:{
          var res = []
          res = res.concat(__libSearchPaths)
          return res
      }
    }
    Probes.PathProbe{
      id:path_of_config
      names:["stm32f4xx_conf.h"]
      pathSuffixes:["", "config", "conf"]
      platformPaths:{
          var res = []
          res = res.concat(__libSearchPaths)
          return res
      }
    }
    //stm32f3xx_hal_conf.h
    property string libPath: {
        var path = "/path_not_found"
        if(path_of_stm32f4periph_lib.found)
            path = path_of_stm32f4periph_lib.path
        return path
    }
    property string configPath: {
        var path = "/config_not_found"
        if(path_of_config.found)
            path = path_of_config.path
        return path
    }


    
    Properties {
        condition:true
        cpp.includePaths: {
            var paths = []
            paths = paths.concat([ periph_lib_path + "/Libraries/CMSIS/Include/",
                                   periph_lib_path + "/Libraries/STM32F4xx_StdPeriph_Driver/inc",
                                   periph_lib_path + "/Libraries/CMSIS/Device/ST/STM32F4xx/Include/",
                                   configPath,
                                  ])
            return paths;
          }

        cpp.staticLibraries:{
                var libs = base
                var name_base = "stm32f4periph_"
                name_base = name_base + MCU
                if(qbs.buildVariant == "debug")
                    name_base = name_base + "d"
                
                libs = libs.concat([name_base])
                return libs
            }

        cpp.libraryPaths:{
            var lib_path = [libPath]
            return lib_path
        }
        cpp.defines:[MCU, "__FPU_PRESENT=1", "USE_STDPERIPH_DRIVER","USE_FULL_ASSERT","STM32F4XX","ARM_MATH_CM4"]


        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections"]
        cpp.driverFlags:["-mthumb", "-mcpu=cortex-m4","-mfloat-abi=hard","-mfpu=fpv4-sp-d16","-specs=nano.specs","-specs=rdimon.specs"]

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++14","-fno-exceptions","-fno-rtti"])
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat(["-std=gnu11"])
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            return flags
        }
    }
}
