import qbs 1.0
import qbs.FileInfo

Module{
    Depends { name: "cpp" }
    property stringList pocoPaths : ["~/projects/cpplibs/poco-1.6.0"]
    property string pocoModuleName
    property string libPath:{
		var paths = []
		for( var i = 0; i < pocoPaths.length; ++i){
			var path = pocoPaths[i] + "/lib"
			if(qbs.targetOS.contains("linux")){
				path = path + "/Linux"
			}
			if(qbs.architecture === "x86")
				path = path + "/x86"
			if(qbs.architecture === "x86_64")
				path = path + "/x86_64"
			if(qbs.architecture === "arm")
				path = path + "/ARMv6"

			paths = paths.concat([path])
		}

		return paths;
    }

    Properties {
        condition:true
        cpp.includePaths: {
            var paths = []
            for( var i = 0; i < pocoPaths.length; ++i){
              paths = paths.concat(pocoPaths[i] + "/" + pocoModuleName + "/include")
            }
            return paths;
          }

        cpp.dynamicLibraries:{
                var dyn_libs = base
                var lib_suffix = ""
                if(qbs.buildVariant == "debug")
                     lib_suffix = "d"
                dyn_libs = dyn_libs.concat(["Poco"+pocoModuleName+lib_suffix])

                return dyn_libs
            }

        cpp.libraryPaths:{
		var lib_path = base
		lib_path = lib_path.concat(libPath)
		return lib_path
        }
        cpp.rpaths : {
		var rpath = base
		rpath = rpath.concat(libPath)
		return rpath
            }
        }
}
